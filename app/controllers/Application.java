package controllers;

import models.AgentInfo;
import models.MQDestination;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import play.*;
import play.cache.Cache;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;

import utils.AgentInfoBuilder;
import utils.MQDestinationBuilder;
import views.html.*;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.*;

public class Application extends Controller {

    private static final int MIN_REFRESH_INTERVAL = 5000;
    private static final String IMQ_COMMAND_LINE = "imqcmd list dst -u admin -pw cadmessaging";
    private static final String OMQ_DEFAULT_PATH = "C:/InterAct/IACAD/MessageBus/bin/";

    public static Result index() {
        return redirect(routes.Application.overview());
    }

    private static String execToString(String command) throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CommandLine commandline = CommandLine.parse(command);
        DefaultExecutor exec = new DefaultExecutor();
        ExecuteWatchdog watchdog = new ExecuteWatchdog(10000);
        exec.setWatchdog(watchdog);
        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
        exec.setStreamHandler(streamHandler);
        exec.execute(commandline);
        return (outputStream.toString());
    }

    private static List<String> removeLines(List<String> lines, int count) {
        for (int x = 0; x < count; x++) {
            String s = lines.remove(0);
            Logger.trace("Removed: " + s);
        }
        return lines;
    }

    private static List<String> removeHeaders(List<String> lines) {
        int removeCount = 0;
        int dashCount = 0;
        boolean ready1 = false;
        Iterator<String> iter1 = lines.iterator();
        while (!ready1 && iter1.hasNext()) {
            String l1 = iter1.next();
            if (StringUtils.startsWith(l1, "---")) {
                dashCount++;
            }
            removeCount++;
            if (dashCount == 2) {
                ready1 = true;
            }
        }

        lines = removeLines(lines, removeCount);
        return lines;
    }

    private static List<String> removeTail(List<String> lines) {
        int count = lines.size();

        for (int x = 1; x <= 3; x++) {
            String s = lines.remove(count - x);
            Logger.trace("Removed: " + s);
        }

        return lines;
    }

    public static List<MQDestination> analyzeJob() {
        String path = Configuration.root().getString("openmq.default.path", OMQ_DEFAULT_PATH);
        String cmdLine = path + IMQ_COMMAND_LINE;

        Logger.trace("Executing: " + cmdLine);

        AgentInfo agentInfo = new AgentInfo();
        List<MQDestination> mqDestinationList = new ArrayList<>();

        try {
            String output = execToString(cmdLine);
            List<String> lines = IOUtils.readLines(new StringReader(output));

            lines = removeHeaders(lines);
            String agentInfoLine = lines.remove(0);
            agentInfo = AgentInfoBuilder.parse(agentInfoLine);

            lines = removeHeaders(lines);
            lines = removeTail(lines);

            for (String l : lines) {
                MQDestination mqd = MQDestinationBuilder.parse(l);
                mqDestinationList.add(mqd);
            }

        } catch (Exception e) {
            Logger.error("Exception when parsing output", e);
        }

        Cache.set("agent-info", agentInfo);
        return mqDestinationList;
    }

    public static Result overviewJson() {
        List<MQDestination> mqDestinationList = (List<MQDestination>) Cache.get("current");
        if (mqDestinationList == null) {
            mqDestinationList = new ArrayList<>();
        }
        return ok(Json.toJson(mqDestinationList));
    }

    public static Result overview() {
        DynamicForm requestData = Form.form().bindFromRequest();

        String refreshMillisStr = requestData.get("refreshMillis");
        String refreshReset = requestData.get("refreshReset");

        if (StringUtils.isNotBlank(refreshReset)) {
            //TODO - Reset cached stats
        }

        int refreshMillis = 0;
        if (StringUtils.isBlank(refreshMillisStr)) {
            Integer tempMillis = (Integer) Cache.get("refreshMillis");
            if (tempMillis != null) {
                refreshMillis = tempMillis;
            }
        } else {
            refreshMillis = Integer.parseInt(refreshMillisStr);
        }

        Logger.trace("Setting cache.refresh: " + refreshMillis);

        if (refreshMillis != 0 && refreshMillis < MIN_REFRESH_INTERVAL) {
            refreshMillis = MIN_REFRESH_INTERVAL;
            flash("message", "Minimum refresh interval was too low.  Setting to " + MIN_REFRESH_INTERVAL);
        }

        Cache.set("refreshMillis", new Integer(refreshMillis));

        AgentInfo agentInfo = (AgentInfo) Cache.get("agent-info");
        if (agentInfo == null) {
            agentInfo = new AgentInfo();
            agentInfo.hostname = "localhost";
            agentInfo.port = "7676";
        }

        List<MQDestination> mqDestinationList = (List<MQDestination>) Cache.get("current");
        if (mqDestinationList == null) {
            mqDestinationList = new ArrayList<>();
        }

        return ok(overview.render(agentInfo, mqDestinationList, refreshMillis));
    }

    public static Result history(String name) {
        if (StringUtils.isBlank(name)) {
            flash("message", "Invalid destination selected");
            return redirect(routes.Application.overview());
        }

        LinkedList<MQDestination> history2 = (LinkedList<MQDestination>) Cache.get(name + ".history");
        if (history2 == null) {
            flash("message", "Destination has no history: " + name);
            return redirect(routes.Application.overview());
        }

        MQDestination mqd = (MQDestination) history2.get(0);
        String type = mqd.destType;
        Logger.debug("Found records: " + history2.size());
        return ok(history.render(name, type, history2));
    }
}