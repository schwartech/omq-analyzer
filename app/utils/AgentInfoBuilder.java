package utils;

import models.AgentInfo;
import play.Logger;

/**
 * Created by jeff on 5/31/16.
 */
public class AgentInfoBuilder {
    private AgentInfoBuilder() {}

    public static AgentInfo parse(String line) {
        AgentInfo agentInfo = new AgentInfo();
        Logger.trace("AgentInfo Line: " + line);

        String[] lineParts = line.split(" ");

        agentInfo.hostname = lineParts[0];
        agentInfo.port = lineParts[lineParts.length-1];

        Logger.trace(agentInfo.toString());

        return agentInfo;
    }
}
