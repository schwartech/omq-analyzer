package utils;

import models.MQDestination;
import org.apache.commons.lang3.StringUtils;
import play.Logger;

import java.math.BigDecimal;

/**
 * Created by jeff on 5/31/16.
 */
public class MQDestinationBuilder {
    private MQDestinationBuilder() {}

    private static int nextIndex(String[] parts, int start) {
        boolean found = false;
        int index = start;
        while (!found && index < parts.length) {
            found = StringUtils.isNotBlank(parts[index]);
            if (!found) {
                index++;
            }
        }

        return index;
    }

    private static Integer getNumber(String value) {
        Integer num;

        if (StringUtils.isNumeric(value)) {
            num = Integer.parseInt(value);
        } else {
            num = null;
        }

        return num;
    }

    private static BigDecimal getBigDecimal(String value) {
        BigDecimal num;

        try {
            num = new BigDecimal(value);
        } catch (NumberFormatException nfe) {
            num = null;
        }

        return num;
    }

    public static MQDestination parse(String line) {
        MQDestination mqd = new MQDestination();
        Logger.trace("MQDestination Line: " + line);

        String[] lineParts = line.split(" ");

        if (Logger.isTraceEnabled()) {
            for (int x = 0; x < lineParts.length; x++) {
                Logger.trace(x + ": " + lineParts[x]);
            }
        }

        mqd.name = lineParts[0];

        int i = 1;
        i = nextIndex(lineParts, i);
        mqd.destType = lineParts[i];

        i++;
        i = nextIndex(lineParts, i);
        mqd.state =  lineParts[i];

        i++;
        i = nextIndex(lineParts, i);
        mqd.totalProducers = getNumber(lineParts[i]);

        i++;
        i = nextIndex(lineParts, i);
        mqd.wildcardProducers = getNumber(lineParts[i]);

        i++;
        i = nextIndex(lineParts, i);
        mqd.totalConsumers = getNumber(lineParts[i]);

        i++;
        i = nextIndex(lineParts, i);
        mqd.wildcardConsumers = getNumber(lineParts[i]);

        i++;
        i = nextIndex(lineParts, i);
        mqd.count = getNumber(lineParts[i]);

        i++;
        i = nextIndex(lineParts, i);
        mqd.remoteMessages = getNumber(lineParts[i]);

        i++;
        i = nextIndex(lineParts, i);
        mqd.unackMessages= getNumber(lineParts[i]);

        i++;
        i = nextIndex(lineParts, i);
        mqd.avgSize = getBigDecimal(lineParts[i]);

        Logger.trace("Found: " + mqd.toString());

        return mqd;
    }
}
