package models;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by jeff on 5/31/16.
 */
public class MQDestination {

    public MQDestination() {
        time = new Date();
    }

    public String name;
    public String destType;
    public String state;
    public Integer totalProducers;
    public Integer wildcardProducers;
    public Integer totalConsumers;
    public Integer wildcardConsumers;
    public Integer count;
    public Integer remoteMessages;
    public Integer unackMessages;
    public BigDecimal avgSize;
    public Date time;

    @Override
    public String toString() {
        return "MQDestination{" +
                "name='" + name + '\'' +
                ", destType='" + destType + '\'' +
                ", state='" + state + '\'' +
                ", totalProducers=" + totalProducers +
                ", wildcardProducers=" + wildcardProducers +
                ", totalConsumers=" + totalConsumers +
                ", wildcardConsumers=" + wildcardConsumers +
                ", count=" + count +
                ", remoteMessages=" + remoteMessages +
                ", unackMessages=" + unackMessages +
                ", avgSize=" + avgSize +
                ", time=" + time +
                '}';
    }
}
