package models;

import java.io.Serializable;

/**
 * Created by jeff on 5/31/16.
 */
public class AgentInfo implements Serializable {

    public String hostname;
    public String port;

    @Override
    public String toString() {
        return "AgentInfo{" +
                "hostname='" + hostname + '\'' +
                ", port='" + port + '\'' +
                '}';
    }
}
