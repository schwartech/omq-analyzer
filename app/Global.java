import controllers.Application;
import akka.actor.Cancellable;
import models.MQDestination;
import play.Configuration;
import play.GlobalSettings;
import play.Logger;
import play.cache.Cache;
import play.libs.Akka;
import scala.concurrent.duration.Duration;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Jeff on 6/1/16.
 */
public class Global extends GlobalSettings {

    Cancellable dataCollector;


    public void onStop(play.Application app) {
        Logger.info("OpenMQ Monitor shutting down");

        if (dataCollector != null && !dataCollector.isCancelled()) {
            Logger.info("* Shutting down OpenMQ Monitor");
            dataCollector.cancel();
        }

    }

    public void onStart(play.Application app) {

        Configuration configRoot = Configuration.root();

        Logger.info("Setting up OpenMQ data collector.");
        dataCollector = Akka.system().scheduler().schedule(
            Duration.create(1, TimeUnit.SECONDS), //Initial delay 10 seconds
            Duration.create(5, TimeUnit.SECONDS),  //Every 1 minute
            new Runnable() {
                public void run() {
                List<MQDestination> results = Application.analyzeJob();
                if (results == null || results.isEmpty()) {
                    Logger.trace("OpenMQ dataCollector found: 0");
                } else {
                    Logger.trace("OpenMQ dataCollector found: " + results.size());

                    Cache.set("current", results);

                    for (MQDestination d : results) {
                        LinkedList<MQDestination> history = (LinkedList<MQDestination>) Cache.get(d.name + ".history");
                        if (history == null) {
                            history = new LinkedList<MQDestination>();
                        }
                        history.add(d);
                        Cache.set(d.name + ".history", history);

                        if (Logger.isTraceEnabled()) {
                            Logger.trace(d.name + ".size: " + history.size());
                        }
                    }

                }
                }
            },
            Akka.system().dispatcher()
        );

    }

}