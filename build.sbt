name := """OpenMQAnalyzer"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
    "org.apache.commons" % "commons-exec" % "1.3",
    "commons-io" % "commons-io" % "2.5",
    cache
)
